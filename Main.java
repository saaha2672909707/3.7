import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

interface Number {//4
    boolean IsNumber(int d);
}
//4
class SumArray{
    public int Sum(int [] A , Number refLambda){
        int sum = 0;
        for (int i = 0; i < A.length; i++)
            if (refLambda.IsNumber(A[i]))
                sum+=A[i];
        return sum;
    }
}
//4

public class Main {



    public static void main(String[] args) {

        List<Integer> setNumbers = new ArrayList<Integer>();//Список елементів
        for (int i = 0; i < 10; i++){
                setNumbers.add((int) (Math.random() * (100 + 1)));
        }
        //1
        System.out.println(setNumbers);
        Object sumSquare = setNumbers.stream()
                .map(i -> i * i).reduce((i1, i2) -> i1 + i2);
        System.out.println(sumSquare);
        //1


        //2
        // вивід парних чисел
        Stream<Integer> stream = setNumbers.stream();
        Predicate<Integer> predicate;
        predicate = (n) -> (n % 2) == 0;
        Stream<Integer> streamFiltered = stream.filter(predicate);
        Iterator<Integer> it = streamFiltered.iterator();
        System.out.println("Filter: ");
        while (it.hasNext()){
            System.out.println(it.next() + " ");
        }
        //кількість парних
        int n2 = (int) (setNumbers.stream()
                .filter((n) ->(n % 2) == 0))
                .count();
        System.out.println("Number of even: " + n2);
        //2


        //3
        Scanner scanner = new Scanner(System.in);
        List<String> setSurnames = new ArrayList<String>();
        System.out.println("Enter last names: ");
        for (int i = 0; i < 5; i++){
            String s = scanner.nextLine();
            setSurnames.add(s);
        }
        System.out.println(setSurnames);
        setSurnames.stream()
                .filter((p) -> p.startsWith("J"))
                .forEach((p) -> System.out.println(p));
        //3

        //4
        Number ref1;
        ref1 = (d) ->{
            if (d%2==0)return true;
            else return false;
        };
        SumArray obj1 = new SumArray();
        int[] setNumber = new int[5];
        for (int i = 0; i < setNumber.length ;i++){
            setNumber[i] = (int) (Math.random() * (100 + 1));
        }
        int sum = obj1.Sum(setNumber,ref1);
        System.out.println("Сума парних: " + sum);

        sum = obj1.Sum(setNumber,
                (Number)((d) ->{
                    if (d %2!=0) return true;
                    else return false;
                }));
        System.out.println("Сума непарних: " + sum);
        //4
        
    }




}